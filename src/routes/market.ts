import {queryManager, defaultPath, PathOptions, beautifyJSON} from "../JS/_functions"
import {Ctx} from "../BnS_API";
import {Market_Current, Market_Current_Top, Market_History, Market_History_Compressed_Day} from "../config/db_entities";

export const market = (ctx: Ctx) => {
	const { database, server } = ctx

	// Returns list of ID's available for /:region/:current
	server.get('/bns/v3/market/:region/:current', async (req, res) => {
		let { beautify,region, current} = queryManager(req)


		let collection;
		switch (current){
			case "Current" : {
				collection = Market_Current;
				break;
			}
			case "History" : {
				collection = Market_History;
				break;
			}
			case "History_archives" : {
				collection = Market_History_Compressed_Day;
				break;
			}
			default: {
				collection = Market_Current;
			}
		}

		let result = await database.get_items(collection, {region: region}, ["id"])
		let processed = result.map(item => item["id"])
		return res.sendRaw(200, beautifyJSON(processed, beautify), { 'Content-Type': "application/json; charset=utf-8" })

	})
	
	server.get('/bns/v3/market/:region/current/:sorting', async (req, res) => {
		let { beautify,region, current, sorting, limit, skip, query} = queryManager(req)

		// this one is returned early
		if (sorting === "lowest") {
			let result = await database.db.collection("Market_Current_Top").aggregate(
				[
					{$match: { region: region } },
					{$unwind: "$listings"},
					{$sort: {"listings.each": 1}},
					{$group: {_id: '$_id','name': {$first: '$name'},'ISO': {$first: '$ISO'},'id': {$first: '$id'},'listings': {$push: '$listings'}}},
					{$sort: {name: 1}},
					{$project: {_id: 0,name: "$name",id: "$id", ISO: "$ISO", totalListings: {$size: "$listings"}, priceEach: {$trunc: {$divide: [{$arrayElemAt: ["$listings.price", 0]}, {$arrayElemAt: ["$listings.count", 0]}]}}, priceTotal: {$arrayElemAt: ["$listings.price", 0]}, quantity: {$arrayElemAt: ["$listings.count", 0]}}}
				]
			).skip(skip).limit(10000).toArray().catch((err) => {console.error(err);return []})

			return res.sendRaw(200, beautifyJSON(result, beautify), {'Content-Type': "application/json; charset=utf-8"})
		}

		let collection, project, sort
		if (sorting === "all") {
			limit = 10000
			query["region"] = region;
			collection = Market_Current
			project = ["name", "id", "ISO", "listings", "listings.price", "listings.count", "listings.each", "totalListings", "totalListed"]
			sort = {name: 1}
		}
		// Now mimics current from befoire the cache was wiped
		if (sorting === "lastlisting") {
			limit = 10000
			query["region"] = region;
			collection = Market_Current_Top
			project = [ "name", "id", "ISO", "listings", "listings.price", "listings.count", "listings.each", "totalListings", "totalListed"]
			sort = {name: 1}
		}

		let id = parseInt(sorting, 10) || 0;
		if(id > 0 ){
			collection = region + current
			project = [ "name","id","ISO", "listings" ,"listings.price", "listings.count", "listings.each","totalListings", "totalListed"]
			sort = {ISO:-1}
			query["id"] = id
			query["region"] = region;
		}

		if(typeof collection === "undefined"){
			return res.sendRaw(200, [],{'Content-Type': "application/json; charset=utf-8"})
		}

		let result = await database.get_items(collection, query, project,skip, limit,  sort );
		return res.sendRaw(200, beautifyJSON(result,beautify),{'Content-Type': "application/json; charset=utf-8"})
	})
	
	server.get('/bns/v3/market/:region/current/top/:sorting', async(req, res) => {
		let { beautify,region, sorting, limit, skip, query} = queryManager(req)


		let sortingQuery = {}
		if(sorting === "price"){ sortingQuery = {"listings.0.price":-1}}
		if(sorting === "quantity"){ sortingQuery = {"totalListed":-1}}
		if(sorting === "listings"){ sortingQuery = {"totalListings":-1}}

		let project = [ "name", "id", "ISO", "listings" ,"listings.price", "listings.count", "listings.each","totalListings", "totalListed"]

		query["region"] = region;

		let result = await database.get_items(Market_Current, query, project, skip, limit, sortingQuery )
		return res.sendRaw(200, beautifyJSON(result,beautify),{'Content-Type': "application/json; charset=utf-8"})
    })

	server.get('/bns/v4/market/:region/history/:sorting', async (req, res) => {
		let { beautify,region, sorting, limit, query, skip} = queryManager(req)

		let optionsRaw:Partial<PathOptions> ={
			limit:limit,
			query:query,
			sort: {ISO:-1},
			beautify:beautify,
			skip: skip
		}
		optionsRaw.query["id"] = parseInt(sorting, 10) || 0
		optionsRaw.query["region"] = region;

		let options:PathOptions = new PathOptions(optionsRaw)
		await defaultPath("json", database, res, req, "Market_History_Compressed_Day", options)
	})
};
