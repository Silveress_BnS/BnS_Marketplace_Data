import {beautifyJSON, queryManager} from "../JS/_functions"
import {Ctx} from "../BnS_API";
import {Item} from "../config/db_entities";

export const status = (ctx: Ctx) => {
	const { database, server } = ctx

	server.get('/bns/v3/status', async (req, res) => {
		let {beautify} = queryManager(req)

		let results = await database.get_items(Item, {id:-1})

		return res.sendRaw(200, beautifyJSON(results,beautify),{'Content-Type': "application/json; charset=utf-8"})
    });

};