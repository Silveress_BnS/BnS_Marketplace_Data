import {Collection,Index} from "./db";




class Main_Abilities {
    element?
    name?: string;
    value: number;
    min: number;
    max: number;
    equip_value: number;
    origin_random_type: number;
    origin_value: number;
    text: string;
    compulsory: boolean;
    max_out: boolean;
    sequence?: number;
}

class Equipment_equip_classes {
    name: string;
    clazz: string;
}

class Equipment_set_item_item_effects_part_effects {
    skill_name?: string;
    description: string;
}

class Equipment_set_item_item_effects {
    part_count: number;
    part_effects: Equipment_set_item_item_effects_part_effects[];
    affected: boolean;
}
class Equipment_set_item {
    name: string;
    grade: number;
    grade_name: string;
    equip_level: number;
    icon: string;
    item_effects: Equipment_set_item_item_effects[]
}

class Appearance {
    id: number;
    name: string;
    level: number;
    icon: string;
    icon_transparent?: string;
    icon_extra: string;
    grade: number;
    grade_name: string;
    set_item_code?: string;
    resealable: boolean;
}

class Equipment_result_equip {
    id: string;
    asset_type: string;
    pos: number;
    equipped_part: string;
    sequestration: boolean;
    sealed: boolean;
    wearable: boolean;
    item: {
        id: number;
        name: string;
        level: number;
        icon: string;
        icon_transparent?: string;
        icon_extra: string;
        grade: number;
        grade_name: string;
        set_item_code?: string;
        resealable: false,
        type: string;
        price: {
            bronze: string;
            silver: string;
            gold: string;
        },
        quoted_price: {
            bronze: string;
            silver: string;
            gold: string;
        }
    }
    appearance?:  Appearance
}





class Item_spirit_Abilities {
    element?
    name: string;
    sequence: number;
    value: number;
    min: number;
    max: number;
    max_out: boolean;
}

class Equipment_result_detail {
    asset_type: string;
    pos: number;
    equipped_part: string;
    sequestration: boolean;
    sealed: boolean;
    guild_id: number;
    used: boolean;
    wearable: boolean;
    durability: number;
    enchant: number;
    quantity: number;
    equip_gem_piece_id: number;
    appearance: boolean;
    added_gems?: Gems[];
    added_enchant_gems?: [];
    gem_identities?
    item: Equipment
    appearance_item?:  Appearance
    growth?: {
        id: number;
        level: number;
        exp: number;
        step: number;
        max_level: number;
        gauge: number;
    }
    custom_list?: number[];
    item_spirit?: {
        id: number;
        grade: number;
        name: string;
        abilities: Item_spirit_Abilities[]
    }
}

export class Result_Equipment {
    [propName: string]: {
        equip: Equipment_result_equip
        detail: Equipment_result_detail
        guild_uniform?
        tooltip_string: string;
    }
}

class Result_Abilities_Base {
    attack_power_value: number;
    attack_pierce_value: number;
    attack_defend_pierce_rate: number;
    attack_parry_pierce_rate: number;
    attack_hit_value: number;
    attack_hit_rate: number;
    attack_concentrate_value: number;
    attack_perfect_parry_damage_rate: number;
    attack_counter_damage_rate: number;
    attack_critical_value: number;
    attack_critical_rate: number;
    attack_stiff_duration_level: number;
    attack_damage_modify_diff: number;
    attack_damage_modify_rate: number;
    hate_power_value: number;
    hate_power_rate: number;
    max_hp: number;
    defend_power_value: number;
    defend_physical_damage_reduce_rate: number;
    aoe_defend_power_value: number;
    aoe_defend_damage_reduce_rate: number;
    defend_dodge_value: number;
    defend_dodge_rate: number;
    counter_damage_reduce_rate: number;
    defend_parry_value: number;
    defend_parry_reduce_rate: number;
    perfect_parry_damage_reduce_rate: number;
    defend_parry_rate: number;
    defend_critical_value: number;
    defend_critical_rate: number;
    defend_critical_damage_rate: number;
    defend_stiff_duration_level: number;
    defend_damage_modify_diff: number;
    defend_damage_modify_rate: number;
    hp_regen: number;
    hp_regen_combat: number;
    heal_power_rate: number;
    heal_power_value: number;
    heal_power_diff: number;
    attack_critical_damage_value: number;
    attack_critical_damage_rate: number;
    abnormal_attack_power_value: number;
    abnormal_attack_power_rate: number;
    pc_attack_power_value: number;
    boss_attack_power_value: number;
    pc_defend_power_value: number;
    pc_defend_power_rate: number;
    boss_defend_power_value: number;
    boss_defend_power_rate: number;
    abnormal_defend_power_value: number;
    abnormal_defend_power_rate: number;
    guard_gauge: number;
    attack_attribute_value: number;
    attack_attribute_rate: number;
    int_attack_power_value: number;
    int_attack_pierce_value: number;
    int_attack_defend_pierce_rate: number;
    int_attack_parry_pierce_rate: number;
    int_attack_hit_value: number;
    int_attack_hit_rate: number;
    int_attack_concentrate_value: number;
    int_attack_perfect_parry_damage_rate: number;
    int_attack_counter_damage_rate: number;
    int_attack_critical_value: number;
    int_attack_critical_rate: number;
    int_attack_critical_damage_rate: number;
    int_attack_stiff_duration_level: number;
    int_attack_damage_modify_diff: number;
    int_attack_damage_modify_rate: number;
    int_hate_power_value: number;
    int_hate_power_rate: number;
    int_max_hp: number;
    int_defend_power_value: number;
    int_defend_physical_damage_reduce_rate: number;
    int_aoe_defend_power_value: number;
    int_aoe_defend_damage_reduce_rate: number;
    int_defend_dodge_value: number;
    int_defend_dodge_rate: number;
    int_counter_damage_reduce_rate: number;
    int_defend_parry_value: number;
    int_defend_parry_reduce_rate: number;
    int_perfect_parry_damage_reduce_rate: number;
    int_defend_parry_rate: number;
    int_defend_critical_value: number;
    int_defend_critical_rate: number;
    int_defend_critical_damage_rate: number;
    int_defend_stiff_duration_level: number;
    int_defend_damage_modify_diff: number;
    int_defend_damage_modify_rate: number;
    int_hp_regen: number;
    int_hp_regen_combat: number;
    int_heal_power_rate: number;
    int_heal_power_value: number;
    int_heal_power_diff: number;
}

class Result_Abilities_point_ability_picks {
    slot: number;
    name: string;
    tier: number;
    point: number;
    description?: string;
}

class Result_Abilities_point_ability {
    offense_point: number;
    defense_point: number;
    attack_power_value: number;
    attack_attribute_value: number;
    max_hp: number;
    defend_power_value: number;
    picks: Result_Abilities_point_ability_picks[]
}

export class Result_Abilities {
    base_ability : Result_Abilities_Base;
    equipped_ability : Result_Abilities_Base;
    total_ability : Result_Abilities_Base;
    point_ability: Result_Abilities_point_ability
}

export class Result_Character {
    // directly from teh character api

    id: number;
    account_id: string;

    name: string;
    race: string;
    gender: string;
    profile_url: string;

    level: number;
    mastery_level: number;

    clazz: string;
    class_name: string;

    ability_achievement_id: number;
    ability_achievement_step: number;

    depository_size: number;
    exp: number;

    faction: string;
    faction_name: string;
    faction_grade: number;
    faction_grade_name: string;
    faction_reputation?: string;
    faction2: string;
    faction2_name?: string;

    gender_name: string;
    geo_zone: number;
    geo_zone_name: string;

    inventory_size: number;

    mastery_exp: number;
    mastery_faction: string;
    mastery_faction_name?: string;

    money: number;

    race_name: string;
    server_id: number;
    server_name: string;
    wardrobe_size: number;
    membership: boolean;

    guild?:{
        guild_id: number;
        guild_name: string;
    }
}

export class Result_Character_Alt {
    account_id: string;
    id: number;
    name: string;
    server_id: number;
    server_name: string;
    clazz: string;
    class_name: string;
    level: number;
    mastery_faction: string;
    mastery_faction_name?: string;
    mastery_level: number;
    last_play_start_time?;
    last_play_end_time?;
    guild?: {
        guild_id: number;
        guild_name: string;
    };
    profile_url: string;
    playing: boolean;
}

// generated to be a rough rundown
export class Character_Equipment {
    [propName: string]: {
        id: number;
        // id of the item its appearing as
        appearance: number;
        gems: number[];
    }
}

class Character_Alt {
    id: number;
    name: string;
    region: string;
}

@Collection()
@Index([{id: 1}, {name: 1, region: 1}, {date_updated_next:1}, {active: 1, date_updated_next:1}])
export class Character {
    id: number;
    account_id: string;
    name: string;
    characters: Character_Alt[];
    active: boolean;

    server_id: number;
    server_name: string;
    region: string;

    date_updated: string;
    date_updated_next: string;
    date_added: string;

    guild_id: number;
    guild_name: string;

    // this has all teh merged data
    base_info: Result_Character;

    abilities: Result_Abilities;

    equipment: Character_Equipment;
}

@Collection()
@Index(["id"])
export class Equipment {
    id: number;
    name: string;
    level: number;
    icon: string;
    icon_transparent?: string;
    icon_extra?: string;
    grade: number;
    grade_name: string;
    set_item_code?: string;
    resealable: boolean;
    type: string;
    soul_shield_type?: string;
    price: number;
    quoted_price: number;
    equip_gender: string;
    equip_race: string;
    equip_level: number;
    equip_faction_name?: string;
    init_durability: number;
    max_durability: number;
    time_limit_type: string;
    disposable: boolean;
    saleable: boolean;
    tradable: boolean;
    storable: boolean;
    auctionable: boolean;
    pc_cafe_only: boolean;
    repair_item_icon?: string;
    repair_item_name?: string;
    equip_classes: Equipment_equip_classes[]
    category_major_name: string;
    category_middle_name: string;
    category_minor_name: string;
    set_item?: Equipment_set_item;
    usable_sub_ability_count: number;
    skill_modify_by_equipment_html?: string;
    main_abilities: Main_Abilities[];
    sub_abilities?: Main_Abilities[]
    caption1_title: string;
    caption2_title: string;
    caption3_title: string;
    caption1_html: string;
    caption2_html: string;
    caption3_html: string;
    description_html: string;
    additional_ability_html: string;
    weapon_appearance_change_type_desc: string;
    modified_skill?
    item_skills: string[]
    enchant_gem_slot_usable1: boolean;
    enchant_gem_slot_usable2: boolean;
    pccafe: boolean;
}

@Collection()
@Index(["id"])
export class Gems {
    id: number;
    name: string;
    slot: number;
    icon: string;
    grade: number;
    grade_name: string;
    category_major_name: string;
    category_middle_name?: string;
    category_minor_name: string;
    main_abilities: Main_Abilities[];
    sub_abilities?: Main_Abilities[];
    icon_transparent: string;
    level: number;
    slot_open: boolean;
}


@Collection()
@Index([{id: 1}, {marketable: 1}])
export class Item {
    id: number;
    category: string;
    updated: string;
    valid?: boolean;

    firstAdded?: string;
    name: string;
    guideUnitPrice: number;
    itemTaxRate: number;
    img: string;
    rank: number;
    merchantValue: number;
    characterLevel: number;
    marketable: boolean;
    class?: string[];
    race?: string;
    sex?: string;
    stats?: string;
}

class Market_Current_listings {
    saleID: number; 
    price: number;
    count: number;
    each: number;
}


export class Market_Base {
    id: number;
    region: string;

    uuid: string;

    ISO: string;
    firstAdded?: string;
    listings: Market_Current_listings[];
    name: string;
    totalListed: number;
    totalListings: number;
}

@Collection()
@Index([{uuid: 1}, {region: 1}, {id: 1, region: 1}])
export class Market_Current extends Market_Base {
}

@Collection()
@Index([{uuid: 1}, {id: 1, region: 1}])
export class Market_Current_Top extends Market_Base {
}

@Collection()
@Index([{uuid: 1}, {id: 1, region: 1}, {id: 1, ISO: 1}, {ISO: 1}])
export class Market_History extends Market_Base {
}

export class Market_History_Compressed_Base {
    id: number;
    region: string;
    uuid: string;
    ISO: string;
    count: number;
    listings_total: number;
    name: string;
    price_avg: number;
    price_max: number;
    price_min: number;
    price_total: number;
    quantity_total: number;
}

@Collection()
@Index([{uuid: 1}, {id: 1, region: 1}])
export class Market_History_Compressed_Day extends Market_History_Compressed_Base {

}

@Collection()
@Index([{uuid: 1}, {id: 1, region: 1}, {delete_date: 1}])
export class Market_History_Compressed_Hour extends Market_History_Compressed_Base {
    delete_date: string;
}

@Collection()
@Index([{ISO: 1}])
export class Count_Character {
    ISO: string;
    total: number;
    eu: number;
    na: number;
    [propName: string]: any;
}

@Collection()
@Index([{ISO: 1}])
export class Count_Equipment {
    ISO: string;
    total: number;
    [propName: string]: any;
}

@Collection()
@Index([{ISO: 1}])
export class Count_Item {
    ISO: string;
    total: number;
}

@Collection()
@Index([{ISO: 1}])
export class Count_Clan {
    ISO: string;
    total: number;
    eu: number;
    na: number;
}

// this is an array of all the entities
export const db_entities: (new () => any)[] = [
    Character,
    Equipment,
    Gems,
    Item,
    Market_Current,
    Market_Current_Top,
    Market_History,
    Market_History_Compressed_Hour,
    Market_History_Compressed_Day,
    Count_Character,
    Count_Equipment,
    Count_Item,
    Count_Clan
];
