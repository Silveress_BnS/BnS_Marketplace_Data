import dotenv from 'dotenv';
const packageData = require("../../../package.json");

dotenv.config();

export const name: string = packageData.name;
export const version: string = packageData.version;
export const env: string = process.env.ENVIROMENT || "production";
export const port: number = parseInt(process.env.PORT, 10) || 84;
export const gpvlu: string = `GPVLU=${process.env.GPVLU}`;

const database = process.env.DB_COLLECTION;
const dbPort = parseInt(process.env.DB_PORT, 10) || 27017;
const dbLocation = process.env.DB_LOCATION;
const dbUser = process.env.DB_USER;
const dbPass = process.env.DB_PASS;

let dbURL = 'mongodb://'+dbUser+':'+dbPass+'@'+dbLocation+':'+dbPort;
if(dbUser === "" || dbPass === "" ){
    dbURL = 'mongodb://'+dbLocation+':'+dbPort;
}

let split = dbLocation.split('.')
if (split.length > 1) {
    dbURL = 'mongodb+srv://' + dbUser + ':' + dbPass + '@' + dbLocation + '/admin?ssl=false'
}

export const db = {
    uri: dbURL,
    database: database
};