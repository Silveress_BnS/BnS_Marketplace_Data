import {MongoClient, Db, FilterQuery} from "mongodb"

export interface MongoIndex {
    [propName: string]: number
}

// not too pretty but works well
export function Collection(collection?: string){
    return function <T extends { new (...args: any[]): {} }>(constructor: T) {
        if(collection){
            // overrides the name of the collection
            if(constructor.name === "Index"){
                return class Collection extends constructor {
                    public collection: string = collection;
                };
            }else{
                return class Collection extends constructor {
                    public collection: string = collection;
                    public indexes: MongoIndex[] = [];
                };
            }
        }else{
            if(constructor.name === "Index"){
                //unchanged
                return class Collection extends constructor {};
            }else{
                return class Collection extends constructor {
                    public collection: string = constructor.name;
                    public indexes: MongoIndex[] = [];
                };
            }
        }

    }
}

export function Index(indexes: MongoIndex[] | string[]){
    let indexes_tmp: MongoIndex[] = [];

    for(let i=0;i<indexes.length;i++){
        let index_sub = indexes[i];
        if(typeof index_sub === "string"){
            indexes_tmp.push({
                [index_sub]: 1
            })
        }else{
            indexes_tmp.push(index_sub)
        }
    }

    return function <T extends { new (...args: any[]): {} }>(constructor: T) {
        return class Index extends constructor {
            public collection: string = constructor.name;
            // once loaded its never changed
            public indexes: MongoIndex[] = indexes_tmp;
        };
    }
}

export interface UpdateOne<T>{
    updateOne: {
        filter: {
            [propName: string]: any
        }
        update: {
            $set: Partial<T>
            $setOnInsert?: Partial<T>
        }
        upsert: boolean
    }
}
export interface DeleteOne<T>{
    deleteOne: {
        filter: {
            [propName: string]: any
        }
    };
}
export interface BulkArray<T>{
    arrays: number
    [propName: number]: (UpdateOne<T>| DeleteOne<T>)[]
}
interface Projection_Sort {
    [propName: string]: number
}
export class DB_Manager {
    // these are public to allow the ability to create custom functions on demand
    public db: Db;
    public client: MongoClient;
    private active: boolean = false;

    constructor(){}

    // management functions
    public async lift(connection_url: string, database: string, db_entities?: {new(): any}[]){
        console.time("Database: Total");
        console.time("Database: Connecting");
        this.client = await MongoClient.connect(connection_url, { useNewUrlParser: true, useUnifiedTopology: true }).catch(async(err) => {err.misc = "MongoDB failed to connect, exiting"; await console.log(err); process.exit(1) });
        this.db = this.client.db(database);
        console.timeEnd("Database: Connecting");

        // set the flag
        this.active = true;

        // these only run if the index.ts is run
        if(db_entities){
            console.time("Database: Creating Collections/Indexes");
            await this.init_db(db_entities);
            console.timeEnd("Database: Creating Collections/Indexes");
        }
        console.timeEnd("Database: Total");
    }

    public async down(){
        await this.client.close();
        // set the flag down
        this.active = false;
    }

    public test_collection<T> (collection: (new () => T) | string): { collection_string: string, indexes: MongoIndex[] }{
        if(typeof collection === "string"){
            return {
                collection_string: collection,
                indexes: []
            }
        }

        try {
            let collection_test = new collection();

            return {
                collection_string: collection_test["collection"],
                indexes: collection_test["indexes"] || []
            }
        } catch{
            return {
                collection_string: "",
                indexes: []
            }
        }
    }

    // indexes/collections
    private async init_db(db_entities: {new(): any}[]){
        for(let i=0;i<db_entities.length;i++){
            let {collection_string, indexes} = this.test_collection(db_entities[i]);
            if (collection_string === ""){continue}

            // set up teh collection
            // if it already exits its noisy
            await this.db.createCollection(collection_string).catch(() => {});

            // then set up teh indexes
            for(let j=0;j<indexes.length;j++){
                await this.db.collection(collection_string).createIndex(indexes[j]).catch(err => console.log(err));
            }
        }
    }



    // this can handle all teh bulk processes
    private async bulk<T>(collection: string, processed: BulkArray<T>, location:Error = new Error()){
        for (let i = 0; i < processed.arrays + 1; i++) {
            if (processed[i].length > 0) {
                await this.db.collection(collection).bulkWrite(processed[i], { ordered: false }).catch((err) => console.log(collection, err,location))
            }
        }
    }

    // takes an array of items, converts it to
    private static bulk_update_array<T> (data:Array<Partial<T>>, limit:number= 100000, accessor:string = "id", setOnInsert?:Partial<T>): BulkArray<T> {
        let tmp: BulkArray<T> = { arrays: 0, 0: [] };
        for (let j = 0; j < data.length; j++) {
            let temp: UpdateOne<T> = {
                updateOne: {
                    filter:{
                        [accessor]: data[j][accessor]
                    },
                    update: {
                        $set: data[j]
                    },
                    upsert: true
                }
            };

            if (setOnInsert) {
                temp.updateOne.update["$setOnInsert"] = setOnInsert
            }
            if (tmp[tmp.arrays].length < limit) {
                tmp[tmp.arrays].push(temp)
            } else {
                tmp.arrays += 1;
                tmp[tmp.arrays] = [temp]
            }
        }
        return tmp
    }

    private static bulk_delete_array<T> (data:T[], limit: number = 1000000, accessor: string = "id"): BulkArray<T> {
        let tmp: BulkArray<T>  = { arrays: 0, 0: [] }

        for (let j = 0; j < data.length; j++) {
            let temp: DeleteOne<T> = {
                deleteOne: {
                    filter: {
                        [accessor]: data[j][accessor]
                    }
                }
            }

            if (tmp[tmp.arrays].length < limit) {
                tmp[tmp.arrays].push(temp)
            } else {
                tmp.arrays += 1
                tmp[tmp.arrays] = [temp]
            }
        }
        return tmp
    }

    // bulk update a lot of individual items
    public async update_bulk<T>(collection: (new () => T) | string, items: Partial<T>[], limit: number = 100000, accessor: string = "id", setOnInsert?:Partial<T>){
        // check if the db is initialised
        if(!this.active){return}

        // first check the collection
        let {collection_string} = this.test_collection(collection)
        if (collection_string === ""){return}


        // take the data
        // bulk it

        let processed = DB_Manager.bulk_update_array(items, limit, accessor, setOnInsert);

        // save it
        await this.bulk(collection_string, processed)
    }

    // ive found that I can go OOM if the quantity of items to delete is too big.
    // this si a workaround
    public async delete_bulk<T>(collection: (new () => T) | string, query: FilterQuery<T>, accessor:string = "id", limit: number = 100000){
        if(!this.active){return}

        // first check the collection
        let {collection_string} = this.test_collection(collection)
        if (collection_string === ""){return}

        let items_to_delete = await this.get_items(collection, query, [accessor]);

        let processed = DB_Manager.bulk_delete_array(items_to_delete, limit, accessor);

        // delete them it
        await this.bulk(collection_string, processed)
    }

    // I really like just passing it in an array of strings and ten generating teh projection
    public process_project(project?: string[]): Projection_Sort{
        // always hide this, unless specifically asked for
        let tmp: Projection_Sort = {
            _id: 0
        }
        if(!project){
            return tmp
        }
        for(let i=0;i<project.length;i++){
            tmp[project[i]] = 1;
        }
        return tmp;
    }

    public async get_items<T>(collection: (new () => T) | string, query: FilterQuery<T>, project?: string[], limit:number=0, skip:number=0, sort: Projection_Sort = {}):Promise<T[]>{
        if(!this.active){return []}

        // first check the collection
        let {collection_string} = this.test_collection(collection);
        if (collection_string === ""){return []}

        let projection = this.process_project(project);

        return this.db
            .collection(collection_string)
            .find(query)
            .project(projection)
            .skip(skip)
            .limit(limit)
            .sort(sort)
            .toArray()

            .catch((err) => {console.log(err);return []});

    }
}