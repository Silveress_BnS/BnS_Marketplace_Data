import {db} from "../config/config";
import {DB_Manager} from "../config/db"
import {Character} from "../config/db_entities";
import {get_character} from "./_functions";

controller().catch((err) => console.error(err));
async function controller() {
    let database = new DB_Manager()
    await database.lift(db.uri, db.database)

    let deleted = []
    let accounts = await database.get_items(Character, {active: true, date_updated_next: {$lte: new Date().toISOString()}})

    let complete = 0;
    let requests = accounts.map(character => get_character(database, character.name, character.region, false, character.id)
        .then((result => {
            complete += 1;
            console.log(`Complete: ${complete}/${accounts.length}`)

            if (typeof result === "undefined") {
                let tmp: Partial<Character> = {
                    id: character.id,
                    active: false,
                }
                deleted.push(tmp)
            }
        })))

    await Promise.all(requests).catch(err => console.log(err))

    if (deleted.length > 0) {
        await database.update_bulk(Character, deleted, undefined, "id")
    }

    await database.down();
}

