import cheerio from 'cheerio'
import axios from'axios'
import { DB_Manager,} from "../config/db"
import {db, gpvlu} from '../config/config'
import {cleanBody} from "./_functions"
import {
	Item,
	Market_Base,
	Market_Current,
	Market_Current_Top,
	Market_History,
	Market_History_Compressed_Hour
} from "../config/db_entities";

let ISO = new Date().toISOString();
let region = process.argv[2].toLowerCase();

// Create arrays for use later
let ids: {[propName: number]: boolean} = {};
let sales: {[propName: number]: boolean} = {};
let names = 0;
let items: Partial<Item>[] = [];
let listings_tmp: {[propName: number]: Market_Base} = {};

controller().catch((err) => console.error(err));
async function controller (){
	let database = new DB_Manager()
	await database.lift(db.uri, db.database)

	// "" is the first page
	await nextPage("");

	await database.update_bulk(Item, items, undefined, "id", {firstAdded: new Date().toISOString()})

	console.log("info \t Clearing Current");
	await database.delete_bulk(Market_Current, {region: region});
	console.log("info \t Cleared Current");

	let listings = Object.values(listings_tmp);
	await database.update_bulk(Market_Current, listings, undefined, "uuid")
	await database.update_bulk(Market_History, listings, undefined, "uuid")

	// update it with a new uuid
	listings = listings.map(listing => {listing.uuid = `${listing.id}_${listing.region}`; return listing})
	await database.update_bulk(Market_Current_Top, listings, undefined, "uuid")

	let listings_hour = listings_to_hour(listings);
	await database.update_bulk(Market_History_Compressed_Hour,listings_hour, undefined, "uuid")

	await database.down();
}

async function nextPage (nextID) {
	let options = {
		url: `http://${region}-bnsmarket.ncsoft.com/bns/bidder/search.web?ct=0-0&exact=&grade=0&level=0-0&prevq=&q=&sort=name-asc&stepper=${nextID}&type=SALE`,
		headers: { 
			'User-Agent': 'BnsIngameBrowser',
			'Cookie': gpvlu
		}
	}
	await axios(options).then(async(response) => await processPage(response,nextID )).catch((err) => {console.error(err)})
}


let count = 0;
async function processPage(response, currentPage){
	let body = cleanBody(response.data)

	let $ = cheerio.load(body);

	// Checks if there is an error in loading teh page, giving it a few attempts before dumping the data
	if (response.status !== 200 || $('.pMarket.error section .message').text().trim() === "We apologize for the inconvenience. Please try again later.") {
		if (count === 5) {
			console.error("Invalid GPVLU");
			// return to teh previous function and up along
			return
		} else {
			count++;
			// wait 10s
			await sleep(10000);
			// then return the function again with the same page
			return nextPage(currentPage)
		}
	}

	// reset it if it gets through
	count = 0

	let rows = $('tr')

	// no rows means that its reached teh end
	if (rows.length === 0) {
		return
	}

	// Find next page
	let next = $('a.next').attr('data-stepper');

	for (let i = 0; i < rows.length; i++) {
		let row = rows.eq(i)

		// Find data strings
		let headerData1 = JSON.parse(row.attr('data-item'));
		let headerData2 = JSON.parse(row.attr('data-sale'));

		// Extract data
		let ID = parseInt(headerData1.item , 10) || 0;
		let name = headerData1.name.replace("&apos;", "'")
		let category = headerData1.ct3;
		let saleID = parseInt(headerData2.sale , 10) || 0;
		let quantity = parseInt(headerData2.amount , 10) || 0;

		let price = parseInt(headerData2.price, 10) || 0;
		let each = price / quantity;

		let item: Partial<Item> =  {
			id: ID,
			name: name,
			category: category,
			updated: ISO,
			marketable: true,
		}
		let figcaption = $('figcaption');
		if (
			figcaption.eq(i).children().length === 2 &&
			figcaption.eq(i).children().last().children().length === 0
		) {
			item.stats = figcaption.eq(i).children().last().text()
		}

		let historyBlank: Market_Base = {
			uuid: `${ID}_${region}_${ISO}`,
			id: ID,
			name: name,
			ISO: ISO,
			region: region,

			listings: [],
			totalListed: 0,
			totalListings: 0
		};

		if (!sales[saleID]) {
			// Add to sales to avoid duplicate entries due to items moving, cant do anythign about missed items.
			sales[saleID] = true;

			if (!ids[ID]) {
				// adding to ids to keep track of what items have the base data added to DB
				ids[ID] = true;

				names += 1;

				//Adding item info to array
				items.push(item);

				// Creating entries for Current and History

				listings_tmp[ID] = historyBlank;
			}

			//console.log(ids.indexOf(ID))
			listings_tmp[ID].listings.push({
				saleID: saleID,
				price: price,
				count: quantity,
				each: each
			});
			listings_tmp[ID].totalListed += quantity;
			listings_tmp[ID].totalListings += 1;

			listings_tmp[ID].listings.sort(compare);

			console.log(`${region}\t${ID}\t${names}\t${name}`)
		}
	}

	if (typeof next === "undefined") {
		console.log(Object.keys(listings_tmp).length);
		return
	}
	await nextPage(next);
}

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

function compare(a,b) {if (a.each < b.each)return -1;if (a.each > b.each)return 1;return 0;}

function listings_to_hour (listings: Market_Base[]):Market_History_Compressed_Hour[]{
	let result = [];

	for (const entry of listings){
		let delete_date_tmp = new Date(entry.ISO)
		// delete date is 8 days in the future from this, wont intermingle with days
		delete_date_tmp.setHours(delete_date_tmp.getHours()+(24*9))
		let delete_date = new Date(delete_date_tmp).toISOString()

		let price_min = Infinity
		let price_max = 0

		let tmp: Market_History_Compressed_Hour = {
			uuid: `${entry.id}_${entry.region}_${entry.ISO}`,
			id: entry.id,
			region: entry.region,
			ISO: entry.ISO,
			name: entry.name,
			delete_date: delete_date,

			count:1,
			price_min:0,
			price_avg:0,
			price_max:0,
			price_total:0,

			quantity_total: 0,

			listings_total:0,
		}

		for(const listing of entry.listings){
			if(listing.each < price_min){tmp.price_min = listing.each}
			if(listing.each > price_max){tmp.price_max = listing.each}

			tmp.price_total += listing.price
			tmp.quantity_total += listing.count
			tmp.listings_total += 1
		}
		tmp.price_avg = Math.round(tmp.price_total/tmp.quantity_total) || 0

		result.push(tmp)
	}

	return result
}