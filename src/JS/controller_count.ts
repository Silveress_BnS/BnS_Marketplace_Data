import { DB_Manager } from "../config/db"
import {
    Character,
    Count_Character,
    Count_Clan,
    Count_Equipment,
    Count_Item,
    Equipment,
    Item
} from "../config/db_entities";
import {db} from "../config/config";

controller().then(() => console.log("Complete")).catch(async(err) => console.error(err));

interface Processing<U, T> {
    input: new () => U,
    project: string[],
    processor: (json: U[]) => T
    output: new () => T
}
async function controller() {
    let database = new DB_Manager()
    await database.lift(db.uri, db.database)

    let details: Processing<any, any>[] = [
        {
            input: Character,
            project: ["region", "base_info.class_name"],
            processor: countCharacters,
            output: Count_Character
        },
        {
            input: Character,
            project: ["guild_id", "region"],
            processor: countClans,
            output: Count_Clan
        },
        {
            input: Equipment,
            project: ["type"],
            processor: countEquipment,
            output: Count_Equipment
        },
        {
            input: Item,
            project: ["id"],
            processor: countItems,
            output:Count_Item
        },
    ]


    for(let item of details){
        await processCount(database,item)
    }

    await database.down();
}

async function processCount<U, T>(database: DB_Manager, item: Processing<U, T>){
    let items = await database.get_items(item.input,{} , item.project)
    if(items.length ===0){ return}

    let data = item.processor(items)
    await database.update_bulk(item.output, [data], undefined, "ISO")
}


const countCharacters = (json: Character[]): Count_Character =>{
    let data: Count_Character = {
        ISO: new Date().toISOString(),
        total: json.length,
        eu: json.filter((item)=> item.region === "eu").length,
        na: json.filter((item)=> item.region === "na").length,
    }

    for(const character of json){
        let player_class = `${character.region} ${character.base_info.class_name}`;
        if(typeof data[player_class] === "undefined"){
            data[player_class] = 1;
        }
        data[player_class] += 1;
    }

    return data
}

const countClans = (json: Character[]): Count_Clan => {
    let tmp: Count_Clan = {
        ISO: new Date().toISOString(),
        total: 0,
        eu: 0,
        na: 0,
    }

    let clans = {};
    for(const character of json){
        if (character.guild_id === 0){continue}
        if (clans[character.guild_id]){continue}

        tmp.total += 1;
        if(typeof tmp[character.region] === "undefined"){
            tmp[character.region] = 0;
        }
        tmp[character.region] += 1;

        clans[character.guild_id] = true;
    }

    return tmp
}

const countEquipment = (json: Equipment[]): Count_Equipment =>{
    let data: Count_Equipment = {
        ISO: new Date().toISOString(),
        total: json.length,
    }

    for(const item of json){
        if(typeof data[item.type] === "undefined"){
            data[item.type] = 1;
        }
        data[item.type] += 1;
    }

    return data
}

const countItems = (json: Item[]): Count_Item => {
    return {
        ISO: new Date().toISOString(),
        total: json.length,
    }
}