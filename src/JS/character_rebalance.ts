import {DB_Manager} from "../config/db";
import {db} from "../config/config";
import {Character} from "../config/db_entities";

controller().catch((err) => console.error(err));
async function controller(){
    console.time("Character: Re-Balance");
    let database = new DB_Manager()
    await database.lift(db.uri, db.database)

    let to_insert = [];
    let characters = await database.get_items(Character, {}, ["id"]);
    // week of seconds divided by teh length
    let time_each = (7*24*60*60)/characters.length

    for(let i=0; i< characters.length;i++){
        let now = new Date();
        now.setSeconds(now.getSeconds() + (time_each * i))
        let character = characters[i];
        let tmp: Partial<Character> = {
            id: character.id,
            date_updated_next: now.toISOString()
        }
        to_insert.push(tmp)
    }
    await database.update_bulk(Character, to_insert)


    await database.down();
    console.timeEnd("Character: Re-Balance");
}
