import {DB_Manager} from "./config/db";
import {db} from "./config/config";
import {
    Character,
    Count_Character,
    Count_Clan,
    Count_Equipment, Count_Item,
    Item, Market_Base,
    Market_Current_Top,
    Market_History, Market_History_Compressed_Base, Market_History_Compressed_Day, Market_History_Compressed_Hour
} from "./config/db_entities";
import {get_character, getNextUpdate} from "./JS/_functions";


controller().catch((err) => console.error(err));
async function controller(){
    console.time("Conversion: Total");
    let database = new DB_Manager()
    await database.lift(db.uri, db.database)

    // characters first as it fills out a few other collections as well
    /* these are run once, first as tehy fill out a few other collections as well
    await characters(database)
    await characterImg(database)
    await characters_added(database)
    await characters_updated_next(database)
    //*/
    await allInfo(database)
    await items(database)

    await characterCount(database)
    await clanCount(database)
    await equipmentCount(database)
    await itemCount(database);

    await euCurrentTop(database)
    await naCurrentTop(database)

    await euHistory(database)
    await naHistory(database)

    await euHistoryCompressed(database)
    await naHistoryCompressed(database)

    await database.down();
    console.timeEnd("Conversion: Total");
}


async function allInfo (database: DB_Manager){
    console.time("Conversion: allInfo");
    let to_insert = [];

    for(const item of await database.get_items("allInfo", {})){
        if(item["eu"]){
            delete item["eu"];
        }
        if(item["na"]){
            delete item["na"];
        }
        to_insert.push(item)
    }

    await database.update_bulk(Item, to_insert, limit, "id")
    console.timeEnd("Conversion: allInfo");
}

async function characterCount (database: DB_Manager){
    console.time("Conversion: characterCount");
    let to_insert = await database.get_items("characterCount", {});
    await database.update_bulk(Count_Character, to_insert, limit, "ISO")
    console.timeEnd("Conversion: characterCount");
}

// characterImg
async function characterImg (database: DB_Manager){
    console.time("Conversion: characterImg");
    let to_insert = [];

    let exists_obj = {
        eu: {},
        na: {}
    }

    let exists_db = {
        eu: {},
        na: {}
    }

    for(const item of await database.get_items("deletedCharacters", {}, ["region", "characterName"])){
        exists_obj[item["region"]][item["characterName"]] = true;
    }

    for(const character of await database.get_items(Character, {}, ["region", "name"])){
        exists_db[character.region][character.name] = true;
    }

    let items = await database.get_items("characterImg", {}, ["region", "name"])
    for(let i=0; i< items.length;i++){
        let item = items[i]
        console.log(i, items.length)

        let name = item["name"];
        let region = item["region"]


        //if(exists_db[region][name]){continue}

        if(exists_obj[region][name]){continue}

        let character = await get_character(database, name, region, true);
        if (typeof character === "undefined" ) {continue}

        exists_obj[region][name] = true;

        // update with original date added
        let tmp: Partial<Character> = {
            id: character.id,
            date_added: item["firstAdded"]
        }
        to_insert.push(tmp)
    }

    await database.update_bulk(Character, to_insert)
    console.timeEnd("Conversion: characterImg");
}

async function characters (database: DB_Manager){
    console.time("Conversion: characters");
    let to_insert = [];

    let exists_db = {
        eu: {},
        na: {}
    }

    for(const character of await database.get_items(Character, {}, ["region", "name"])){
        exists_db[character.region][character.name] = true;
    }

    for(const item of await database.get_items("deletedCharacters", {})){
        let name = item["characterName"];
        let region = item["region"]


        if(exists_db[region][name]){continue}

        let character = await get_character(database, name, region, true);
        if (typeof character === "undefined" ) {continue}
        // update with original date added
        let tmp: Partial<Character> = {
            id: character.id,
            date_added: item["firstAdded"]
        }
        to_insert.push(tmp)
    }

    await database.update_bulk(Character, to_insert)
    console.timeEnd("Conversion: characters");
}

// to fix the date added
// imgur removed at 2019-06-30T06:17:37Z
async function characters_added (database: DB_Manager){
    console.time("Conversion: characters_added");
    let to_insert = [];

    let deleted_characters = {
        eu: {},
        na: {}
    }

    for(const item of await database.get_items("deletedCharacters", {}, ["characterName", "region", "firstAdded"])){
        let name = item["characterName"];
        let region = item["region"];
        let added = item["firstAdded"];

        if(!deleted_characters[region][name]){
            deleted_characters[region][name] = added
        }
    }

    for(const character of await database.get_items(Character, {date_added: null}, ["id", "name", "region"])){
        let date;
        if(typeof deleted_characters[character.region][character.name] !== "undefined"){
            // if the name/region combo matches the deleted characters use their date
            date = deleted_characters[character.region][character.name];
        } else {
            /*
            This is the date I stopped using the imgur links
            https://gitlab.com/Silver_BnS/BnS_Marketplace_Data/-/commit/78bfb85d644955004fa535ee6555e379ae341e0b
             */
            date = "2019-06-30T06:17:37Z"
        }
        let tmp: Partial<Character> = {
            id: character.id,
            date_added: date
        }
        to_insert.push(tmp)
    }

    await database.update_bulk(Character, to_insert)
    console.timeEnd("Conversion: characters_added");
}

async function characters_updated_next (database: DB_Manager){
    console.time("Conversion: characters_updated_next");

    // rebalance the update times
    let to_insert = [];
    for(const character of await database.get_items(Character, {}, ["id"])){
        let tmp: Partial<Character> = {
            id: character.id,
            date_updated_next: getNextUpdate(true, "")
        }
        to_insert.push(tmp)
    }
    await database.update_bulk(Character, to_insert)
    console.timeEnd("Conversion: characters_updated_next");
}

async function clanCount (database: DB_Manager){
    console.time("Conversion: clanCount");
    let to_insert = await database.get_items("clanCount", {});
    await database.update_bulk(Count_Clan, to_insert, limit, "ISO")
    console.timeEnd("Conversion: clanCount");
}

// equipment
// this may not be able to transfer over nicely
// maybe use the img/icon?

async function equipmentCount (database: DB_Manager){
    console.time("Conversion: equipmentCount");
    let to_insert = [];

    for(const item of await database.get_items("equipmentCount", {})){
        item["total"] = item["totalEquipmentCount"]
        delete item["totalEquipmentCount"];
        to_insert.push(item)
    }

    await database.update_bulk(Count_Equipment, to_insert, limit, "ISO")
    console.timeEnd("Conversion: equipmentCount");
}

// euCurrentTop
// naCurrentTop

async function euCurrentTop (database: DB_Manager){
    console.time("Conversion: euCurrentTop");
    let to_insert = [];

    for(const item of await database.get_items("euCurrentTop", {})){
        if(typeof item["uuid"] === "undefined"){
            item["uuid"] = `${item["ISO"]}_${item["id"]}`
        }
        let processed = currentTop(item, "eu")
        to_insert.push(processed)
    }

    await database.update_bulk(Market_Current_Top, to_insert, limit, "uuid")
    console.timeEnd("Conversion: euCurrentTop");
}

async function naCurrentTop (database: DB_Manager){
    console.time("Conversion: naCurrentTop");
    let to_insert = [];

    for(const item of await database.get_items("naCurrentTop", {})){
        if(typeof item["uuid"] === "undefined"){
            item["uuid"] = `${item["ISO"]}_${item["id"]}`
        }
        let processed = currentTop(item, "na")
        to_insert.push(processed)
    }

    await database.update_bulk(Market_Current_Top, to_insert, limit, "uuid")
    console.timeEnd("Conversion: naCurrentTop");
}


function currentTop (old, region: string): Market_Base{
    let tmp: Market_Current_Top = {
        ISO: old["ISO"],
        id: old["id"],
        listings: old["listings"],
        name: old["name"],
        region: region,
        totalListed: old["totalListed"],
        totalListings: old["totalListings"],
        firstAdded: old["firstAdded"],
        uuid: old["uuid"]
    }

    if(tmp.firstAdded === ""){
        tmp.firstAdded = new Date().toISOString();
    }
    if(typeof tmp.firstAdded === "undefined"){
        tmp.firstAdded = new Date().toISOString();
    }

    // create the new uuid
    tmp.uuid = split_uuid(tmp.uuid, region);

    return tmp
}

function split_uuid(uuid: string, region: string): string{
    let uuid_split = uuid.split("_");
    let uuid_new = [uuid_split[0], region, uuid_split[1]];
    return uuid_new.join("_");
}
// euHistory
// naHistory


let limit = 100000;
async function euHistory (database: DB_Manager){
    await history(database, "eu")
}

async function naHistory (database: DB_Manager){
    await history(database, "na")
}

async function history(database: DB_Manager, region: string){
    console.time(`Conversion: ${region}History`);

    let total = 0;
    let skip = 0;

    // incrimentally update it
    let date_array = await database.get_items(Market_History, {region: region}, undefined, 1, 0, {ISO: -1});
    let date = date_array[0].ISO;

    let items = await database.get_items(`${region}History`, {ISO: {$gte:date }}, undefined, limit, skip);
    while (items.length > 0){
        let to_insert = [];
        for(const item of items){
            if(typeof item["uuid"] === "undefined"){
                item["uuid"] = `${item["ISO"]}_${item["id"]}`
            }
            let processed = currentTop(item, region)
            to_insert.push(processed)
            total += 1;
        }
        await database.update_bulk(Market_History, to_insert, limit, "uuid")

        // prep for next time
        skip += limit;
        items = await database.get_items(`${region}History`, {ISO: {$gte:date }}, undefined, limit, skip);
    }

    console.log(`Total: ${total}`)

    console.timeEnd(`Conversion: ${region}History`);
}


// euHistoryCompressed
// naHistoryCompressed
async function euHistoryCompressed (database: DB_Manager){
    console.time("Conversion: euHistoryCompressed");
    let to_insert_hour = [];
    let to_insert_day = [];

    for(const item of await database.get_items("euHistoryCompressed", {type: "hour"})){
        if(typeof item["uuid"] === "undefined"){continue}
        let processed:Partial<Market_History_Compressed_Hour>  = historyCompressed(item, "eu")
        processed.delete_date = item["delete_date"];
        to_insert_hour.push(processed)
    }

    for(const item of await database.get_items("euHistoryCompressed", {type: "day"})){
        if(typeof item["uuid"] === "undefined"){continue}
        let processed:Market_History_Compressed_Day  = historyCompressed(item, "eu")
        to_insert_day.push(processed)
    }

    await database.update_bulk(Market_History_Compressed_Hour, to_insert_hour, limit, "uuid")
    await database.update_bulk(Market_History_Compressed_Day, to_insert_day, limit, "uuid")
    console.timeEnd("Conversion: euHistoryCompressed");
}

async function naHistoryCompressed (database: DB_Manager){
    console.time("Conversion: naHistoryCompressed");
    let to_insert_hour = [];
    let to_insert_day = [];

    for(const item of await database.get_items("naHistoryCompressed", {type: "hour"})){
        if(typeof item["uuid"] === "undefined"){continue}
        let processed:Partial<Market_History_Compressed_Hour>  = historyCompressed(item, "eu")
        processed.delete_date = item["delete_date"];
        to_insert_hour.push(processed)
    }

    for(const item of await database.get_items("naHistoryCompressed", {type: "day"})){
        if(typeof item["uuid"] === "undefined"){continue}
        let processed:Market_History_Compressed_Day  = historyCompressed(item, "eu")
        to_insert_day.push(processed)
    }

    await database.update_bulk(Market_History_Compressed_Hour, to_insert_hour, limit, "uuid")
    await database.update_bulk(Market_History_Compressed_Day, to_insert_day, limit, "uuid")
    console.timeEnd("Conversion: naHistoryCompressed");
}

function historyCompressed (old, region: string): Market_History_Compressed_Base{
    let tmp: Market_History_Compressed_Base = {
        id: old["id"],
        uuid: old["uuid"],
        ISO: old["ISO"],
        name: old["name"],
        region: region,
        count: old["count"],
        listings_total: old["listings_total"],
        price_avg: old["price_avg"],
        price_max: old["price_max"],
        price_min: old["price_min"],
        price_total: old["price_total"],
        quantity_total: old["quantity_total"],
    }

    // create the new uuid
    tmp.uuid = split_uuid(tmp.uuid, region);

    return tmp
}

// itemCount
async function itemCount (database: DB_Manager){
    console.time("Conversion: itemCount");
    let to_insert = [];

    for(const item of await database.get_items("itemCount", {})){
        item["total"] = item["itemCount"]
        delete item["itemCount"];
        to_insert.push(item)
    }

    await database.update_bulk(Count_Item, to_insert, limit, "ISO")
    console.timeEnd("Conversion: itemCount");
}

async function items (database: DB_Manager){
    console.time("Conversion: items");
    let to_insert = [];

    for(const item of await database.get_items("items", {})){
        let tmp: Partial<Item> = {
            id: item["id"],
            firstAdded: item["firstAdded"]
        }
        to_insert.push(tmp)
    }

    await database.update_bulk(Count_Equipment, to_insert, limit, "ISO")
    console.timeEnd("Conversion: items");
}
